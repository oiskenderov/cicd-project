FROM nginx:1.23.3-alpine
COPY source/index.html /usr/share/nginx/html/
EXPOSE 80
