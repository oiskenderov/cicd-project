function DataStorage () {
    this.socket = null;
    this.storage = null;
}
DataStorage.prototype.getSocket = function () {
    return this.socket;
}
DataStorage.prototype.setSocket = function (socket) {
    this.socket = socket;
};
DataStorage.prototype.getStorage = function () {
    return this.storage;
};
DataStorage.prototype.setStorage = function (storage) {
    this.storage = storage;
};
DataStorage.prototype.methodget = function (message) {
    var key = message.key;
    if (!key) {
        return null;
    }
    try {
        return this.storage.get(key);
    } catch (e) {
        return null;
    }
};
DataStorage.prototype.methodset = function (message) {
    var key = message.key,
        value = message.value;

    if (!key) {
        return null;
    }
    try {
        this.storage.set(key, value);
        return this.storage.get(key);
    } catch (e) {
        return null;
    }
};
DataStorage.prototype.receiveMessage = function (message, origin) {
    message = JSON.parse(message);
    var type = 'method' + message.type;
    if (typeof this[type] === 'function') {
        message.value = this[type](message);
        this.socket.postMessage(JSON.stringify(message));
    }
};